FROM httpd:2.4

LABEL maintainer="Ibad Haider"
LABEL email="ibad.haider@mail.utoronto.ca"
LABEL version="0.1"
RUN apt-get update
RUN apt -y install mysql-server
COPY startup.sh /startup.sh
RUN chmod +x /startup.sh
COPY index.html /usr/local/apache2/htdocs

ENTRYPOINT ["/startup.sh"] 